import { GameHeader } from "../components/GameHeader";


export function GameAds() {
  return (
    <div className='max-w-[1344px] mx-auto flex flex-col items-start my-20'>
      <GameHeader
        bannerUrl={"https://static-cdn.jtvnw.net/ttv-boxart/32399_IGDB-285x380.jpg"}
        title={"Counter Strike: GO"}/>
    </div>
  )
}